const { Builder, By, Key, until } = require('selenium-webdriver');

(async function example() {
  try {
    let driver = await new Builder().forBrowser('chrome').build();
    await driver.get('https://chisel-markdown1.netlify.com/');
    // await driver.findElement(By.name('q')).sendKeys('webdriver', Key.RETURN);
    await driver.wait(until.titleIs('- Chisel Markdown'), 10000);
    const screenShot = await driver.takeScreenshot();
    require('fs').writeFileSync(
      './screenshot.png',
      Buffer.from(screenShot, 'base64')
    );
    await driver.quit();
  } catch (e) {
    console.log(e);
  }
})();
